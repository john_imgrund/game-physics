﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceGenerator
{
    // f = mg
    public static Vector2 GenerateForce_Gravity(float particleMass, float gravitationalConstant, Vector2 worldUp)
    {
        Vector2 f_gravity = particleMass * gravitationalConstant * worldUp;
        return f_gravity;
    }

    // f_normal = proj(-f_gravity, surfaceNormal_unit)
    public static Vector2 GenerateForce_normal(Vector2 f_gravity, Vector2 surfaceNormal_unit)
    {
        Vector2 f_normal = Vector3.Project(new Vector3(f_gravity.x, f_gravity.y, 0.0f), surfaceNormal_unit);
        return -f_normal;
    }

    // f_sliding = f_gravity + f_normal
    public static Vector2 GenerateForce_sliding(Vector2 f_gravity, Vector2 f_normal)
    {
        Vector2 f_sliding = f_gravity + f_normal;
        return f_sliding;
    }

    // f_friction_s = -f_opposing if less than max, else -coeff*f_normal (max amount is coeff*|f_normal|)
    public static Vector2 GenerateForce_friction_static(Vector2 f_normal, Vector2 f_opposing, float frictionCoefficient_static)
    {
        Vector2 f_friction_s = f_opposing.magnitude < f_normal.magnitude * frictionCoefficient_static ? -f_opposing : -frictionCoefficient_static * f_normal;
        return f_friction_s;
    }

    // f_friction_k = -coeff*|f_normal| * unit(vel)
    public static Vector2 GenerateForce_friction_kinetic(Vector2 f_normal, Vector2 particleVelocity, float frictionCoefficient_kinetic)
    {
        Vector2 f_friction_k = f_normal.magnitude * frictionCoefficient_kinetic * particleVelocity;
        return f_friction_k;
    }

    // f_drag = (p * u^2 * area * coeff)/2
    public static Vector2 GenerateForce_drag(Vector2 particleVelocity, Vector2 fluidVelocity, float fluidDensity, float objectArea_crossSection, float objectDragCoefficient)
    {
        //calculate u
        Vector2 particleSpeed = particleVelocity / fluidVelocity;

        Vector2 f_drag = (fluidDensity * (particleSpeed * particleSpeed) * objectArea_crossSection * objectDragCoefficient) / 2.0f;
        return f_drag;
    }

    // f_spring = -coeff*(spring length - spring resting length)
    public static Vector2 GenerateForce_spring(Vector2 particlePosition, Vector2 anchorPosition, float springRestingLength, float springStiffnessCoefficient)
    {
        Vector2 springForce = anchorPosition - particlePosition;

        //calc magnitude
        float springMagnitude = springForce.magnitude;
        springMagnitude = Mathf.Abs(springMagnitude - springRestingLength);
        springMagnitude *= springStiffnessCoefficient;

        springForce.Normalize();
        springForce *= springMagnitude;

        return springForce;

        //Vector2 f_spring = -springStiffnessCoefficient * (springForce  -  springRestingLength);
    }
}
