﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

//Written by John Imgrund and Parker Staszkiewicz

    public enum Test { none, gravity, sliding, friction, drag, spring };

public class Particle2D : MonoBehaviour
{
    //Step 1 (Define Particles)
    public Vector2 position, velocity, acceleration;
    public float rotation, angularVelocity, angularAcceleration;

    private Vector2 surfaceNormal = new Vector2(0, 1);
    public Vector2 fluidVelocity = new Vector2(1, 5);
    public Vector2 anchor = new Vector2(0, 3);

    Test test = Test.none;

    //Lab 2 - Step 1
    public float startingMass = 1.0f;
    float mass, massInv;

    public void SetMass(float newMass)
    {
        mass = newMass > 0.0f ? newMass : 0.0f;

        //Similar to above
        //mass = Mathf.Max(0.0f, newMass);

        massInv = newMass > 0.0f ? 1.0f / newMass : 0.0f;
    }

    public float GetMass()
    {
        return mass;
    }

    //Lab 2 - Step 2
    public Vector2 force;

    public void AddForce(Vector2 newForce)
    {
        //D'Alembert
        force += newForce;
    }

    void UpdateAcceleration()
    {
        //Newton 2
        acceleration = massInv * force;

        force.Set(0.0f, 0.0f);
    }

    //step 2 (Integration Algorithms)
    void UpdatePositionExplicitEuler(float dt)
    {
        // x(t + dt) = x(t) + v(t) * dt
        //Euler's Method:
        //F(t + dt) = F(t) + f(t) * dt
        //                 + (dF / dt) * dt
        position += velocity * dt;

        //v(t + dt) = v + a(d) * dt
        velocity += acceleration * dt;
    }

    void UpdatePositionKinematic(float dt)
    {
        //x(t + dt) = x(t) + v(t) * dt + .5 * a(t) * (dt * dt)
        position += velocity * dt + .5f * acceleration * (dt * dt);

        //v(t + dt) = v + a(d) * dt
        velocity += acceleration * dt;
    }

    void UpdateRotationEuler(float dt)
    {
        rotation += angularVelocity * dt;
        angularVelocity += angularAcceleration * dt;
    }

    void UpdateRotationKinematic(float dt)
    {
        rotation += angularVelocity * dt + .5f * angularAcceleration * (dt * dt);

        angularVelocity += angularAcceleration * dt;
    }

    public void SetRotation(float r)
    {
        transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, r));
    }

    // Start is called before the first frame update
    void Start()
    {
        //Lab 2
        SetMass(startingMass);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //integrate
        //CheckInput();

        UpdatePositionExplicitEuler(Time.fixedDeltaTime);
        UpdateRotationEuler(Time.fixedDeltaTime);
        UpdateAcceleration();

        //apply to transform
        transform.position = position;

        Vector2 f_gravity = ForceGenerator.GenerateForce_Gravity(mass, -9.8f, Vector2.up);
        Vector2 f_normal = ForceGenerator.GenerateForce_normal(f_gravity, surfaceNormal);
        Vector2 f_sliding = ForceGenerator.GenerateForce_sliding(f_gravity, f_normal);
        Vector2 f_friction_static = ForceGenerator.GenerateForce_friction_static(transform.up, -transform.up, 0.5f);
        Vector2 f_friction_kinetic = ForceGenerator.GenerateForce_friction_kinetic(transform.up, velocity, 0.1f);
        Vector2 f_drag = ForceGenerator.GenerateForce_drag(velocity, fluidVelocity, 1, 0.25f, 0.25f);
        Vector2 f_spring = ForceGenerator.GenerateForce_spring(transform.position, anchor, 1.0f, 0.5f);

        switch (test)
        {
            case Test.none:
                break;
            case Test.gravity:
                AddForce(f_gravity);
                break;
            case Test.sliding:
                AddForce(f_sliding);
                break;
            case Test.friction:
                AddForce(f_sliding);
                AddForce(f_friction_static);
                //AddForce(f_friction_kinetic); // Kinetic without static results in no movement?
                break;
            case Test.drag:
                AddForce(f_sliding);
                AddForce(f_friction_static);
                AddForce(f_drag);
                break;
            case Test.spring:
                AddForce(f_spring);
                break;
            default:
                break;
        }
    }

    public void SetTest(Test t)
    {
        test = t;
    }
}